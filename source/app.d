module app; 
import vibe.vibe;
import logger = std.experimental.logger; 
import std.traits; 
import mir.random; 
import mir.random.algorithm; 
//import mir.random.engine.xorshift; 
//import mir.random.engine.mersenne_twister;
//import mir.random.engine.linear_congruential;
//import std.random; 
import std.range;
import cellulect; 
import std.typecons; 

/*
float St = 8; // mole 
float Et = 4; // mole 
float k1 = 2; // 1/(mole * second)
float k1r = 1; // 1/second --> 0.4 
float k2 = 1.5; // 1/second --> 0.6
*/

//float St = 5; // mole 
//float Et = 1; // mole 
float k1 = 30; // 1/(mole * second)
float k1r = 1; // 1/second --> 0.4 
float k2 = 10; // 1/second --> 0.6

auto timeSteps(float St, float Et, float k1, float k1r, float k2)
{
	import std.math : ceil; 

	return cast(size_t)ceil(P1(St, Et, k1) + k1r + k2 + 1); 
}

auto P1(float St, float Et, float k1)
{
	return k1 * (St + Et); 
}

struct KParams
{
	float k1; 
	float k1r; 
	float k2; 
	float k3; 
	float k3r; 
	float K; // k2/k1 Van Slyke-Cullen constant
	float Ka; // k1r/k1 = Kd // dissociation equilibrium constant
	float Km; // (k1r + k2)/k1 = K + Kd = K + Ka Michaelis-Menten constant
	float Kic;  // k3r/k3 
}

auto normalizingFactor(float St, float Et)
{
	return 1/(P1(St, Et, k1) + k1 *  Km(k1, k1r, k2)); 
}

auto KToP(T)(T kParams) if(__traits(isPOD, T))
{

}
auto Km(float k1, float k1r, float k2)
{
	return (k1r + k2) / k1; 
}
/*
static this() //shared
{
	import etc.linux.memoryerror;
	static if (is(typeof(registerMemoryErrorHandler)))
		registerMemoryErrorHandler();

	auto router = new URLRouter; 
	router.get("*", staticTemplate!"index.dt"); 
	router.post("/upload", &upload);
	auto settings = new HTTPServerSettings;
	settings.port = 8080;
	
	settings.bindAddresses = ["::1", "0.0.0.0"];
	//listenHTTP(settings, &hello);
	listenHTTP(settings, router); 

	//logInfo("Please open http://127.0.0.1:8080/ in your browser.");
	logInfo("Server running");
	//runApplication(); // no run application command as the predefined main method is used. 
}
*/

void main()
{
	uint[10] seedArr; 
	// fill the seed Arr;

	for(auto i = 0; i < seedArr.length; ++i)
	{
		seedArr[i] = cast(uint)unpredictableSeed; 
	}
	
	//assert(seedArr.length == 10); 
	//logger.log("5. seed: ", seedArr[4]); 

	// 100, 500, 1000, 5000, 10000
	// 10000, 120000, 230000, 340000, 450000, 560000, 670000, 780000, 890000, 1000000
	size_t[] monAmounts = [10000]; //[1000, 10000, 100000]; 
	//size_t monAmount = monAmounts[1]; 
	//float[] Sts = [5, 10, 12.5, 15, 17.5, 20, 25, 30, 50, 100]; 
	//float[] Sts = [25]; 
	float[] Ets = [1.0]; //[4.0, 4.2, 4.4, 4.6, 4.8, 5.0, 5.2, 5.4, 5.6, 5.8, 6.0]; 

	Tuple!(float, "St", float, "Et")[] materials; 
	for(auto i = 0; i < Ets.length; ++i)
	{
		materials ~= Tuple!(float, "St", float, "Et")(Ets[i] * 5.0, Ets[i]); 
		//materials[i].St = Ets[i] * 5.0; 
		//materials[i].Et = Ets[i]; 
	}
	foreach(i, uS; seedArr)
	{
		logger.log("processing ", i, ". seed"); 
		foreach(j, monAmount; monAmounts)
		{
			logger.log("processing ", j, ". monAmount");
			foreach(k, material; materials)
			{
				logger.log("processing ", k, ". material combination");
				Bulk bulk = Bulk(monAmount); 
		
				size_t rounds = timeSteps(material.St, material.Et, k1, k1r, k2); //551 + 1; 

				
				size_t[][] statsP; 
				statsP.length = rounds; 
				
				foreach(ref s; statsP) 
					s.length = monAmount; 
				size_t[] statsE; 
				statsE.length = rounds; 

				size_t round; 

				void cut(size_t mon = monArray.length) @nogc
				{
					//logger.log("calling cut in app, mon: ", mon); 
					if(mon == monArray.length) // init
					{ 
						size_t lastMon; 
						while(lastMon < monArray.length)
						{
							auto res = bulk.getPolymer(lastMon); 
							statsP[0][res.length - 1] += 1; 
							lastMon += res.length; 
						}
					}
					else
					{
						auto res = bulk.getPolymer(mon);
						//logger.log("res.length: ", res.length); 
						//logger.log("statsP[round][res.length - 1]: ", statsP[round][res.length - 1]);
						--statsP[round][res.length - 1]; 
						//logger.log("mon: ", mon); 
						//logger.log("mon - res.front: ", mon - res.front); 
						//logger.log("mon: ", mon); 
						//logger.log("res.front: ", res.front); 
						//logger.log("res.back: ", res.back);
						//logger.log("in: ", res.front in bulk); 
						++statsP[round][mon - res.front - 1];
						//logger.log("res.back - mon + 1: ", res.back - mon + 1); 
						++statsP[round][res.back - mon]; 
						bulk.cut(mon);
					}
				}

				void adsorb() @nogc
				{
					--statsE[round]; 
				}

				void desorb() @nogc
				{
					++statsE[round]; 
				}

				cut();

				auto rndGen = Random(uS);  //Mt19937(uS & Mt19937.max);//Xorshift(uS); 
				//logger.log("unpredictable seed was: ", uS); 

				Enzyme[] enzymes; 
				enzymes.length = monAmount/5;
				foreach(ref e; enzymes)
					e = Enzyme((P1(material.St, material.Et, k1)/(k1r + k2 + P1(material.St, material.Et, k1))), (1.0/(k1r + k2)));//(24.0/26.5, 0.4);//(9.6/10.6,
	 //0.4); //(44.0/53.0,
					//2.0/3.0); 

				statsE[0] = enzymes.length; 
				//logger.log("checkpoint 1");
				foreach(r; 1 .. rounds)
				{
					round = r; 

					statsP[round][] = statsP[round - 1]; 
					statsE[round] = statsE[round - 1];
					rndGen.shuffle(enzymes); 
					//enzymes.randomShuffle(rndGen); 

					foreach(ref enzyme; enzymes)
						enzyme.opCall!(typeof(rndGen), typeof(bulk))(rndGen, bulk, &cut, &adsorb, &desorb, statsE[round]); 

				}

				FS fileStruct; 
				fileStruct.monNumber = monAmount; 
				fileStruct.seed = uS; 
				fileStruct.St = material.St; 
				fileStruct.Et = material.Et; 
				writeStats(material.St, statsP, material.Et, statsE, fileStruct);
				//logger.log(normalizingFactor); 
			}
		}
	}
	//logger.log("rounds needed: ", timeSteps(material.St, material.Et, k1, k1r, k2)); 
	//logger.log("P1: ", P1(material.St, material.Et, k1)); 
}

struct FS
{

	size_t monNumber;
	size_t seed;
	float St; 
	float Et; 
	string generateFileName(T...)(T arr)
	{
		import std.conv; 
		string s; 
		static if(T.length)
		{
			foreach(el; arr[0])
			{
				foreach(i, ref part; this.tupleof)
				{
					if(__traits(identifier, this.tupleof[i]) == el)
						if(i) s ~= "_"; 
						s ~= __traits(identifier, this.tupleof[i]); 
						s ~= "_"; 
						s ~= to!(string)(part); 
				}
			}
			assert(arr[0].length == FieldNameTuple!(typeof(this)).length); 
		}
		else // the standard combination
		{
			foreach(i, ref part; this.tupleof)
			{
				if(__traits(identifier, this.tupleof[i]))
					if(i) s ~= "_";
					s ~= __traits(identifier, this.tupleof[i]); 
					s ~="_";
					s ~= to!(string)(part); 
			}
			import std.datetime : Clock; 
			s ~= "_"; 
			s ~= "time"; 
			s ~= "_";
			s ~= split(Clock.currTime().toISOString, ".")[0]; 
		}
		
		return s;
	}
}
/*
auto writeDataStructure(FS fileStruct)
{
	import std.algorithm; 
	import std.array; 
	import std.file; 
	import std.path; 

	string pathPrefix = "./output/structure"; 
	auto res = permutations([FieldNameTuple!FS]); 
	foreach(r; res)
	{
		auto massive = fileStruct.generateFileNameSuffix(r).split('_').filterBidirectional!(a => !a.empty).enumerate; 
		assert(!(massive.count & 1)); 

		string currentPath; 
		foreach(i, s; massive)
		{
			if(!(i & 1))
			{
				currentPath ~= s; 
			}
			else//((i & 1))
			{
				currentPath ~= "_" ~ s; 
				//mkdir(pathPrefix ~ dirSeparator ~ massive[i-1] ~ "_" ~ massive[i]);
				if(!exists(pathPrefix ~ dirSeparator ~ currentPath))
					mkdir(pathPrefix ~ dirSeparator ~ currentPath); 
				currentPath ~= dirSeparator; 
				//logger.log("i: ", i); 
				//logger.log(s); 
			}
				
			//logger.log(fileStruct.generateFileNameSuffix(r)); 
		}
		version(Posix)
		{
			logger.log("./output/data/file"~fileStruct.generateFileNameSuffix()); 
			assert(exists("./output/data/file"~fileStruct.generateFileNameSuffix())); 
			logger.log(absolutePath("output/data/file"~fileStruct.generateFileNameSuffix())); 
			symlink(absolutePath("output/data/file"~fileStruct.generateFileNameSuffix()), 
				pathPrefix ~ dirSeparator ~ currentPath ~ "file"); 
		}
	}
}
*/
void writeStats(float St, ref size_t[][] statsP, float Et, ref size_t[] statsE, FS fileStruct)
{
	
	
	
	import std.stdio : writeln, File;  
    auto f = File("./output/data/current/"~fileStruct.generateFileName() , "wa");
    
    size_t[] cums; 
    cums.length = statsP.length; 
    size_t[] polymerAmounts; 
    polymerAmounts.length = statsP.length; 
    foreach(step, ref stat; statsP)
    	foreach(l, ref s; stat)
    	{
    		polymerAmounts[step] += s;
    		if(l > 6)
    			cums[step] += s * (l + 1); 
    	}
    

    /*		
    foreach(step, ref stat; statsP)
	{
    	f.writeln(
    		step, "\t",
    		stat[0] * 1, "\t",
    		stat[1] * 2, "\t",
    		stat[2] * 3, "\t",
    		stat[3] * 4, "\t",
    		stat[4] * 5, "\t",
    		stat[6] * 6, "\t",
    		cums[step]
    	);
	}
	*/
	foreach(step, ref stat; statsP)
	{
		f.writeln(
			step * normalizingFactor(St, Et), "\t", 
			(cast(float)polymerAmounts[step] - cast(float)polymerAmounts[0])/cast(float)monArray.length * St, "\t", 
			(cast(float)monArray.length - cast(float)polymerAmounts[step])/cast(float)monArray.length * St, "\t", 
			cast(float)statsE[step]/statsE[0] * Et

		);
	}
    f.close(); 
    //writeDataStructure(fileStruct); 
}

void hello(HTTPServerRequest req, HTTPServerResponse res)
{
	res.writeBody("Hello, World!");
}

void index(HTTPServerRequest req, HTTPServerResponse res)
{
	res.render!("index.dt"); 
}

void user(HTTPServerRequest req, HTTPServerResponse res)
{
	auto user_id = req.params["id"]; 
	string name = "John Doe"; 
	int age = 25; 

	res.render!("user.dt", user_id, name, age); 
}

void upload(HTTPServerRequest req, HTTPServerResponse res)
{
	import std.stdio; 
	auto title = req.form.get("title"); 
	logInfo("hmm");
	
	auto file = "document" in req.files; 
	try
	{
		moveFile(file.tempPath, Path("./public/uploads") ~ file.filename); 
		writeln("Uploaded successfully!"); 
	}
	catch(Exception e)
	{
		writeln("Exception thrown, trying copy"); 
		copyFile(file.tempPath, Path("./public/uploads") ~ file.filename); 
	}
	
	logInfo("Form title is: " ~ title); 
	writeln("Form title is: ", title); 
	res.redirect("/");
}
